This is the repository for the Plasma 5 Firewall KCM.

# Requirements
- CoreAddons.
- KCMUtils.
- I18n.
- Plasma 5.8.4+.
- PlasmaQuick.
- KDELibs4Support.
- Declarative.

# Issues
If you find problems with the contents of this repository please create an issue.

