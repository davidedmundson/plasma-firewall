
/*
 * Copyright 2018 Alexis Lopes Zubeta <contact@azubieta.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.6
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.9 as QQC2

import org.kde.kcm 1.2 as KCM
import org.kcm.firewall 1.0

import org.kde.kirigami 2.10 as Kirigami

KCM.ScrollViewKCM {
    id: root

    implicitHeight: Kirigami.Units.gridUnit * 22

    KCM.ConfigModule.quickHelp: i18n("This module lets you configure firewall.")

    property var policyChoices : [
        {text: i18n("Allow"), data: "allow"},
        {text: i18n("Deny"), data: "deny"},
        {text: i18n("Reject"), data: "reject"}
    ]
    function getCurrentIndex(data, model) {
        for (var i = 0; i < model.length; i++) {
            if (model[i].data === data) {
                return i;
            }
        }
        return 0;
    }

    UfwClient {
        id: ufwClient
        logsAutoRefresh: false
    }

    NetstatClient {
        id: netStatClient
    }

    Kirigami.OverlaySheet {
        id: drawer
        parent: root.parent

        RuleEdit {
            id: ruleEdit
            height: childrenRect.height
            implicitWidth: 30 * Kirigami.Units.gridUnit
            onAccept: drawer.close()
            defaultOutgoingPolicyRule: policyChoices[defaultOutgoingPolicy.currentIndex].data
            defaultIncomingPolicyRule: policyChoices[defaultIncomingPolicy.currentIndex].data
        }
    }

    header: ColumnLayout {
        id: columnLayout
        Kirigami.InlineMessage {
            id: netstatError
            type: Kirigami.MessageType.Information
            text: netStatClient.status !== ""
            visible: netStatClient.status !== ""
            Layout.fillWidth: true
        }

        Kirigami.InlineMessage {
            id: ufwError
            type: Kirigami.MessageType.Information
            text: ufwClient.status
            visible: ufwClient.status !== ""
            Layout.fillWidth: true
        }
        Kirigami.FormLayout {
            QQC2.CheckBox {
                Kirigami.FormData.label: "Firewall Status:"
                text: ufwClient.enabled ? i18n("Enabled") : i18n("Disabled")
                onClicked: ufwClient.enabled = !ufwClient.enabled
                checked: ufwClient.enabled
            }

            QQC2.ComboBox {
                id: defaultIncomingPolicy
                Kirigami.FormData.label: "Default Incomming Policy:"
                model: policyChoices
                textRole: "text"
                currentIndex: getCurrentIndex(ufwClient.defaultIncomingPolicy, policyChoices)
                onCurrentIndexChanged: ufwClient.defaultIncomingPolicy = policyChoices[currentIndex].data
                enabled: ufwClient.enabled
            }

            QQC2.ComboBox {
                id: defaultOutgoingPolicy
                Kirigami.FormData.label: "Default Incomming Policy:"
                model: policyChoices
                textRole: "text"
                currentIndex: getCurrentIndex(ufwClient.defaultOutgoingPolicy, policyChoices)
                onCurrentIndexChanged: ufwClient.defaultOutgoingPolicy = policyChoices[currentIndex].data
                enabled: ufwClient.enabled
            }
        }
    }

    view: ListView {
        model: ufwClient.rules()
        delegate: RuleListItem {
            onEdit: {
                var rule = ufwClient.getRule(index)
                ruleEdit.rule = rule
                ruleEdit.newRule = false
                drawer.open()
            }
            onRemove: {
                ufwClient.removeRule(index)
            }
        }
    }

    footer: RowLayout {
        QQC2.Button {
            text: i18n("Connections...")
            onClicked: kcm.push("ConnectionsView.qml", {
                "drawer": drawer,
                "ufwClient": ufwClient,
                "netStatClient" : netStatClient,
                "ruleEdit": ruleEdit,
            });
        }
        QQC2.Button {
            text: i18n("Logs...")
            onClicked: kcm.push("LogsView.qml", {
                "drawer": drawer,
                "ufwClient": ufwClient,
                "ruleEdit": ruleEdit,
            });
        }
        Item {
            Layout.fillWidth: true
        }
        QQC2.Button {
            enabled: ufwClient.enabled
            height: 48
            icon.name: "list-add"
            text: i18n("New Rule")
            onClicked: drawer.open()
        }
    }
}
