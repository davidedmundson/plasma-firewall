
/*
 * Copyright 2018 Alexis Lopes Zubeta <contact@azubieta.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.12 as QQC2
import org.kde.kcm 1.2 as KCM

KCM.ScrollViewKCM {
    id: root
    property var drawer: null
    property var ufwClient: null
    property var ruleEdit: null

    onUfwClientChanged: {
        ufwClient.logsAutoRefresh = true
    }

    onVisibleChanged: {
        ufwClient.logsAutoRefresh = visible
    }

    view: ListView {
        model: ufwClient.logs()
        delegate: LogItemDelegate {
            id: itemRoot
            width: root.width - 10
            onFilterLog: {
                var rule = ufwClient.createRuleFromLog(protocol, sourceAddress, sourcePort, destinationAddress, destinationPort, iface)
                ruleEdit.rule
                ruleEdit.newRule = true
                drawer.open();
            }
        }

        add: Transition {
            NumberAnimation { property: "opacity"; from: 0; to: 1.0; duration: 400 }
        }
    }
}
