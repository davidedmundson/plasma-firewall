import QtQuick 2.12
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.1

import QtQuick.Controls 2.12 as QQC2
import org.kde.kirigami 2.4 as Kirigami

import org.kcm.firewall 1.0 as Firewall

Kirigami.FormLayout {
    signal accept(var rule)
    signal reject()

    property var defaultOutgoingPolicyRule: null
    property var defaultIncomingPolicyRule: null

    property var rule: Firewall.Rule {
        policy: "deny"
        incoming: true
        logging: "none"
    }

    property var ruleChoices : [
        {text: i18n("None"), data: "none"},
        {text: i18n("New Connections"), data: "log"},
        {text: i18n("All Packets"), data: "log-all"}
    ]
    property bool newRule: false

    onAccept: {
        if (newRule)
            ufwClient.addRule(rule)
        else
            ufwClient.updateRule(rule)
    }

    QQC2.ComboBox {
        id: policy
        Kirigami.FormData.label: "Policy:"
        model: policyChoices
        textRole: "text"
        currentIndex: getCurrentIndex(rule.policy, policyChoices)
        onCurrentIndexChanged: rule.policy = policyChoices[currentIndex].data
    }

    RowLayout {
        Kirigami.FormData.label: "Direction:"

        QQC2.Button {
            id: incoming
            text: i18n("Incoming")
            icon.name: "arrow-down"
            checked: rule.incoming
            onClicked: rule.incoming = true
        }
        QQC2.Button {
            text: i18n("Outgoing")
            icon.name: "arrow-up"
            checked: !rule.incoming
            onClicked: rule.incoming = false
        }
    }

    GridLayout {
        Kirigami.FormData.label: "Source:"

        columns: 3
        QQC2.Label {
            text: i18n("Address:")
        }
        IpV4TextField {
            id: sourceAddress
            text: rule.sourceAddress
            onTextChanged: rule.sourceAddress = text

            //TODO: Move this to KConfigXT
            property var originalValue
            Component.onCompleted: originalValue = rule.sourceAddress
        }

        QQC2.CheckBox {
            text: i18n("Any")
            checked: sourceAddress.text == ""
                        || sourceAddress.text == "0.0.0.0/0"
            onClicked: sourceAddress.text = checked ? "" : sourceAddress.originalValue
        }
        QQC2.Label {
            text: i18n("Port:")
        }
        PortTextField{
            id: sourcePort
            text: rule.sourcePort
            onTextChanged: rule.sourcePort = text

            //TODO: Move this to KConfigXT
            property var originalValue
            Component.onCompleted: originalValue = rule.sourcePort
        }
        QQC2.CheckBox {
            text: i18n("Any")
            Layout.fillWidth: true

            checked: sourcePort.text == "" || sourcePort.text == "0/0"
            onClicked: sourcePort.text = checked ? "" : sourcePort.originalValue
        }
    }

    GridLayout {
        Kirigami.FormData.label: "Destination:"

        columns: 3
        QQC2.Label {
            text: i18n("Address:")
        }
        IpV4TextField {
            id: destinationAddress
            text: rule.destinationAddress
            onTextChanged: rule.destinationAddress = text
        }
        QQC2.CheckBox {
            text: i18n("Any")
            checked: destinationAddress.text == ""
                        || destinationAddress.text == "0.0.0.0/0"
        }
        QQC2.Label {
            text: i18n("Port:")
        }
        PortTextField {
            id: destinationPort
            placeholderText: "0/0"
            text: rule.destinationPort
            onTextChanged: rule.destinationPort = text
        }
        QQC2.CheckBox {
            text: i18n("Any")
            checked: destinationPort.text == ""
                        || destinationPort.text == "0/0"

            Layout.fillWidth: true
        }
    }

    QQC2.ComboBox {
        Kirigami.FormData.label: "Protocol:"

        id: protocolCb

        model: ufwClient.getKnownProtocols()

        currentIndex: rule.protocol
        onCurrentIndexChanged: rule.protocol = currentIndex
    }
    QQC2.ComboBox {
        Kirigami.FormData.label: "Interface:"

        id: interfaceCb

        model: ufwClient.getKnownInterfaces()

        currentIndex: rule.interface
        onCurrentIndexChanged: rule.interface = currentIndex

    }

    QQC2.ComboBox {
        Kirigami.FormData.label: "Logging:"
        model: ruleChoices
        textRole: "text"
        currentIndex: getCurrentIndex(rule.logging, ruleChoices)
        onCurrentIndexChanged: rule.logging = ruleChoices[currentIndex].data
    }

    Item {
        Layout.fillHeight: true
    }
    RowLayout {
        QQC2.Button {
            text: i18n("Accept")
            icon.name: "dialog-ok"
            enabled: (!incoming.checked && policyChoices[policy.currentIndex].data !== defaultOutgoingPolicyRule)
                  || (incoming.checked && policyChoices[policy.currentIndex].data !== defaultIncomingPolicyRule)
            onClicked: accept(rule)
        }
    }
}
